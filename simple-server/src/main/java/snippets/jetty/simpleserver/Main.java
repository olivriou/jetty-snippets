package snippets.jetty.simpleserver;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;

public class Main
{

	public static void main(final String[] args) throws Exception
	{
		final Server server = new Server();
		final ServerConnector connector = new ServerConnector(server);
		connector.setPort(8080);
		connector.setReuseAddress(true);
		server.setConnectors(new Connector[] { connector });
		server.setHandler(new SimpleHandler());
		server.start();

		server.join();
	}

}
