package snippets.jetty.simplejerseyservice;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.server.ServerProperties;

public class Main
{
	public static void main(final String[] args) throws Exception
	{
		final ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		context.setContextPath("/api");
		final ServletHolder jerseyServlet = context.addServlet(org.glassfish.jersey.servlet.ServletContainer.class, "/*");
		jerseyServlet.setInitOrder(0);
		jerseyServlet.setInitParameter(ServerProperties.PROVIDER_CLASSNAMES, String.join(",", SimpleJerseyService.class.getCanonicalName()));

		final Server server = new Server();
		final ServerConnector connector = new ServerConnector(server);
		connector.setPort(8080);
		connector.setReuseAddress(true);
		server.setConnectors(new Connector[] { connector });

		server.setHandler(context);

		server.start();

		server.join();
	}
}
