package snippets.jetty.simplejerseyservice;

public class Message
{
	private String text;

	public String getText()
	{
		return this.text;
	}

	public void setText(final String value)
	{
		this.text = value;
	}
}
