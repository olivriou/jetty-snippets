package snippets.jetty.simplejerseyservice;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/simple-service")
public class SimpleJerseyService
{

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Message getMessage()
	{
		return MessageDao.INSTANCE.getMessage();
	}

	@POST
	@Path("/update")
	@Consumes(MediaType.APPLICATION_JSON)
	public void setMessage(final Message msg)
	{
		MessageDao.INSTANCE.setMessage(msg);
	}
}
