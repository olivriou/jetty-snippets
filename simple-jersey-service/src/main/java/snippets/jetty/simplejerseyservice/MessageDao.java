package snippets.jetty.simplejerseyservice;

public class MessageDao
{

	public static final MessageDao INSTANCE = new MessageDao();

	private Message message;

	public Message getMessage()
	{
		return this.message;
	}

	public void setMessage(final Message msg)
	{
		this.message = msg;
	}
}
